<?php

namespace App;

class Player
{
    /** @var string */
    public $name;

    /** @var int */
    public $points = 0;

    /**
     * Player constructor.
     *
     * @param string $name
     */
    public function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * Player scores a point.
     *
     * @void
     */
    public function score(): void
    {
        $this->points++;
    }

    /**
     * Name a current point.
     *
     * @return string
     */
    public function toScore(): string
    {
        switch ($this->points) {
            case 0:
                return 'love';
            case 1:
                return 'fifteen';
            case 2:
                return 'thirty';
            case 3:
                return 'forty';
        }
    }
}
