<?php

namespace App;

class TennisMatch
{
    /** @var Player */
    protected $playerOne;

    /** @var Player */
    protected $playerTwo;

    /**
     * TennisMatch constructor.
     *
     * @param Player $playerOne
     * @param Player $playerTwo
     */
    public function __construct(Player $playerOne, Player $playerTwo)
    {
        $this->playerOne = $playerOne;
        $this->playerTwo = $playerTwo;
    }

    /**
     * Name a score.
     *
     * @return string
     */
    public function score()
    {
        if ($this->hasWinner()) {
            return sprintf("Winner: %s", $this->leader()->name);
        }

        if ($this->hasAdvantage()) {
            return sprintf("Advantage: %s", $this->leader()->name);
        }

        if ($this->isDeuce()) {
            return 'deuce';
        }

        return sprintf(
            "%s-%s",
            $this->playerOne->toScore(),
            $this->playerTwo->toScore()
        );
    }

    /**
     * Check whether the game is ended.
     *
     * @return bool
     */
    protected function hasWinner(): bool
    {
        if (max([$this->playerOne->points, $this->playerTwo->points]) < 4) {
            return false;
        }

        return abs($this->playerOne->points - $this->playerTwo->points) >= 2;
    }

    /**
     * Get the current leader of the set.
     *
     * @return Player|null
     */
    protected function leader(): ?Player
    {
        if ($this->playerOne->points == $this->playerTwo->points) {
            return null;
        }

        return $this->playerOne->points > $this->playerTwo->points
            ? $this->playerOne
            : $this->playerTwo;
    }

    /**
     * @return bool
     */
    protected function isDeuce(): bool
    {
        return $this->hasReachedDeuceThreshold() && $this->playerOne->points == $this->playerTwo->points;
    }

    /**
     * Determine if both players have scored at least 3 points.
     *
     * @return bool
     */
    protected function hasReachedDeuceThreshold(): bool
    {
        return $this->playerOne->points >= 3 && $this->playerTwo->points >= 3;
    }

    /**
     * Determine if one player has the advantage.
     *
     * @return bool
     */
    protected function hasAdvantage() :bool
    {
        if ($this->hasReachedDeuceThreshold()) {
            return ! $this->isDeuce();
        }

        return false;
    }
}
