<?php

namespace App;

class FizzBuzz
{
    /**
     * Convert number with FizzBuzz algorithm.
     *
     * @param int $number
     * @return string
     */
    public static function convert(int $number): string
    {
        $string = '';

        if ($number % 3 == 0) {
            $string .= 'fizz';
        }

        if ($number % 5 == 0) {
            $string .= 'buzz';
        }

        return $string ?: $number;
    }
}
