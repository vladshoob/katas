<?php

namespace App;

use Exception;

class StringCalculator
{
    /**
     * Define value past which values are ignored.
     */
    const MAX_NUMBER_ALLOWED = 1000;

    /**
     * Basic delimiter for string parsing
     * if other is not specified.
     *
     * @var string
     */
    protected $delimiter = ",|\n";

    /**
     * Given string is parsed and summed up accordingly to rules.
     *
     * @param string $numbers
     * @return int
     * @throws Exception
     */
    public function add(string $numbers): int
    {
        $this->disallowNegatives(
            $numbers = $this->parseString($numbers)
        );

        return array_sum($this->ignoreOutOfRangeValues($numbers));
    }

    /**
     * Parse string to array of numbers.
     *
     * @param string $numbers
     * @return array
     */
    protected function parseString(string $numbers): array
    {
        $customDelimiter = "\/\/(.)\n";

        if (preg_match("/{$customDelimiter}/", $numbers, $matches)) {
            $this->delimiter = $matches[1];
            $numbers = str_replace($matches[0], '', $numbers);
        }

        $numbers = preg_split("/{$this->delimiter}/", $numbers);

        return $numbers;
    }

    /**
     * If any negatives encountered,
     * then throw an exception.
     *
     * @param array $numbers
     * @throws Exception
     */
    protected function disallowNegatives(array $numbers): void
    {
        foreach ($numbers as $number) {
            if ($number < 0) {
                throw new Exception();
            }
        }
    }

    /**
     * Ignore values if they are exceeding set limit.
     *
     * @param array $numbers
     * @return array|string
     */
    protected function ignoreOutOfRangeValues(array  $numbers): array
    {
        return array_filter($numbers, function ($number) {
            return $number <= self::MAX_NUMBER_ALLOWED;
        });
    }
}
